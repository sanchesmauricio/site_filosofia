<?php include "head.php"; ?>
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="page-header">
           <h2>Seja bem vindo!</h2>
         </div>
            <p class="inicio">Olá estudantes, vocês também tem <b>amor à sabedoria</b>? Se sim, você está no lugar certo!
                Esse blog é basicamente dedicado ao nosso grande filósofo <b>Aristóteles</b>.
                Se você o conhece, sabe que ele foi um <b>grande pensador</b> de sua época e que merece nosso 
                <b>respeito e homenagem</b>.
                Por isso nós (Dayane de Lima,Marina Beatriz,Victor Freitas,Fernando Bessone,Pedro Paulo,Lorenzo Montenegro e Maurício Sanches), 
                esperamos encarecidamente que vocês <b>gostem</b>, e principalmente se <b>divirtam</b> com o intuito dessa 
                página.
                <br>
                Nesse blog você irá encontrar :
            <ul id="ul">
                <li>Curiosidades relacionadas a Aristóteles;</li>
                <li>Sua biografia;</li>
                <li>Frases, e o que ele pensava em sua época;</li>
                <li>Notícias;</li>
                <li>Texto elaborado pelos donos do Blog.</li>
            </ul>
            </p> 
          </div>
        </div>
      </div>

        <div class="container">
         <div class="panel panel-default">
          <div class="panel-body">
           <h2>A importância dos jovens na política</h2>
           <h4>Confiram abaixo o texto feito por nós do blog <span class="glyphicon glyphicon-hand-down" aria-hidden="true"></span> </h4>
           
            <p class="inicio">A participação da juventude na política é de extrema importância para trazer novas ideias e construir um novo caminho. Nossos jovens tem que acreditar na força como o instrumento de transformação.</p>

            <p class="inicio">Nossa juventude deveria ser mais carregada de debates políticos, um erro dos políticos é tratar os jovens como se fossem um conjunto único.</p>

            <p class="inicio">As pessoas mais novas ajudam sim a mudar a qualidade de vida do nosso país, optando principalmente pela decisão política.</p>

            <p class="inicio">Pelo que vemos os jovens quase não se preocupam com o que vem pela frente, mas deveriam se preocupar e muito! Estamos em 2018, um ano de eleições, por esse fato os mais novos tem que ter a consciência de que se o  país continuar da mesma maneira, não iremos a lugar algum! Apenas continuaremos atolados.</p>

            <p class="inicio">Resumindo, os jovens realmente ajudam a mudar, nem que seja pelo menos um pouco. Então, vamos nos conscientizar e ter certeza, de que a pessoa que iremos votar seja boa para o país.</p>

          </div>
          <div class="panel-footer">Publicação feita no dia 14/05/2018 </div>
        </div>
        

        <div class="panel panel-default">
          <div class="panel-body">
           <h2>Arqueólogos gregos acreditam ter encontrado túmulo de Aristóteles</h2>
          
           <img alt="Busto de Aristóteles (Foto: National Museum of Rome)" 
           class="img-responsive img-rounded"  src="http://s2.glbimg.com/LCgz8dD8mgYHlF7fagYqrao2y34=/s.glbimg.com/jo/g1/f/original/2016/05/26/aristotle.jpg" title="Busto de Aristóteles (Foto: National Museum of Rome)" id="aristoteles1">
       
            <p class="inicio">"Não temos provas, mas indícios muito fortes de que beiram a certeza", declarou o diretor das escavações, Konstandinos Sismanidis, a veículos de imprensa locais.

            Sismanidis apresentou hoje os resultados no congresso internacional "Aristóteles - 2.400 anos", realizado na Universidade de Salônica.

            A equipe em torno de Sismanidis chegou à conclusão de que uma construção descoberta em 1996 nas citadas escavações não pode ser outra coisa que o mausoléu de Aristóteles, após analisar dois manuscritos que faziam alusão à transferência das cinzas do filósofo para sua cidade natal.

            Os arqueólogos que trabalhavam em Estagira desde o início dos anos 1990 ficaram surpresos que no meio de uma fortificação do período bizantino houvesse destroços de uma edificação, cujas características não coincidiam com essa época nem com eras posteriores.
            
            As descobertas no interior das ruínas da construção - moedas de Alexandre, o Grande, e de seus sucessores - situam seu erguimento no começo do período helenístico.

            Os destroços do teto achados neste sítio arqueológico demonstraram que a construção tinha sido coberta com telhas da fábrica real, o que demonstra que se tratava de um prédio público.

            O local fica entre uma galeria do século V a.C. e um templo de Zeus do século VI a.C., dentro da antiga cidade, perto de sua ágora, e com vista panorâmica.

            No piso do local há um retângulo de 1,30 por 1,70 metro, o que corresponde a um altar.

            <img  src="https://i0.wp.com/res.cloudinary.com/aleteia/image/fetch/c_fill,g_auto,w_620,h_310/https://aleteiaportuguese.files.wordpress.com/2016/06/b3f0e125067e3a0c7bc9f0885af88ec8.jpg?resize=620%2C310&amp;quality=100&amp;strip=all&amp;ssl=1" alt="Resultado de imagem para tumulo de aristoteles" onload="typeof google==='object'&amp;&amp;google.aft&amp;&amp;google.aft(this)" id="img-tumulo" class="img-responsive img-rounded">


            Todas estas indicações e o fato de que a forma da construção não permitia atribuir-lhe outro uso que o de um túmulo, fizeram os arqueólogos suspeitar de que se tratava de um mausoléu.

            Finalmente, chegaram à conclusão que provavelmente a pessoa à qual era dedicado o mausoléu era Aristóteles, com a ajuda de dois documentos antigos: uma tradução em árabe do século XI d.C. de uma biografia do filósofo grego e o manuscrito N. 257 da Biblioteca Nazionale Marciana, de Veneza.

            Ambos os documentos dizem que quando Aristóteles morreu em 322 a.C. na cidade de Calcis (atual Calcídia) os moradores de Estagira transferiram suas cinzas para uma urna de cobre, a puseram em um mausoléu e a ao lado delas construíram um altar.</p>   
          </div>
          <div class="panel-footer">Publicação feita no dia 10/05/2018 <p>Reportagem do site: <a href="http://g1.globo.com/mundo/noticia/2016/05/arqueologos-gregos-acreditam-ter-encontrado-tumulo-de-aristoteles.html" target="_blank">www.g1globo.com</a></div>
        </div>

        <div class="panel panel-default">
          <div class="panel-body">
           
               <h2>Pensamento de Aristóteles segue influente no mundo contemporâneo</h2>

              
               <img src="http://1.bp.blogspot.com/-t9d5EFEnFP4/T_w9bKOv_QI/AAAAAAAABaE/Dt8_y8O9v0M/s1600/etica2.jpg" alt="Resultado de imagem para mundo contemporâneo aristoteles" onload="typeof google==='object'&amp;&amp;google.aft&amp;&amp;google.aft(this)" class="img-responsive img-rounded" id="img-mundo2">
      
            <p class="inicio" align="center">Estética, ética, metafísica, astronomia, biologia e filosofia foram alguns dos temas e dos campos de interesse do pensador grego Aristóteles, ainda no século IV a.C.  Muitas de suas reflexões foram pioneiras e serviram como ponto de partida para os conhecimentos atuais. Outras, porém, permanecem relevantes, seja porque são obras clássicas e merecem a leitura, seja porque ainda permanecem atuais.

           “A influência do pensamento de Aristóteles nos dias de hoje é muito maior do que imaginamos. A nossa forma de compreender o mundo é influenciada pelo pensamento de Aristóteles. A própria forma como estruturamos o nosso raciocínio é profundamente influenciada por Aristóteles, inclusive quando consideramos que estamos tirando uma conclusão. Isso que chamamos hoje de raciocínio lógico tem uma forte influência de Aristóteles”, diz a doutora em Filosofia pela Universidade Federal do Rio de Janeiro (UFRJ) Cíntia Martins Dias.

           
            <img alt="Globo Ciência: Aristóteles (Foto: Reprodução TV)" src="http://s.glbimg.com/og/rg/f/original/2011/10/05/poetica_291x218.jpg" title="Globo Ciência: Aristóteles (Foto: Reprodução TV)" class="img-responsive img-rounded" id="img-mundo">
       

           O raciocínio lógico a que Cíntia se refere está relacionado ao que ficou conhecido como lógica aristotélica, por meio da qual é possível se tirar conclusões a partir de premissas verdadeiras, dentre outras leis que permitiriam a dedução. Aristóteles não foi apenas o pioneiro no estudo da lógica, como suas reflexões nesta área continuam válidas.

           Ele foi o primeiro a mostrar como funciona o raciocínio. Todo o estudo de lógica parte da lógica clássica de Aristóteles. Todo o procedimento de raciocínio, de tirar conclusões, de generalizações, quem estudou explicitamente pela primeira vez foi Aristóteles”, diz Cíntia.

            Aristóteles também se preocupou em estabelecer os princípios gerais para se fazer uma ciência. De acordo com a professora titular de Filosofia da Natureza da Universidade do Estado do Rio de Janeiro (Uerj), Elena Moraes Garcia, o grego concluiu que, para isso, era preciso a existência de “princípios”, de “causas” e de “elementos”, o que influenciou toda a concepção de ciência posteriormente.

           </p>   
          </div>

          <div class="panel-footer">Publicação feita no dia 10/05/2018 <p>Reportagem do site: <a href="http://redeglobo.globo.com/globociencia/noticia/2011/10/pensamento-de-aristoteles-segue-influente-no-mundo-contemporaneo.html" target="_blank">www.redeglobo.com</a></p>
          </div>


 <?php include "footer.php"; ?>