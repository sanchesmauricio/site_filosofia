<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Blog do Filósofo - Melhor blog de Filosofia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/estilo.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#barra-navegacao" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php"><span class="cor">Blog</span> do <span class="cor">Filósofo</span></a>
            </div>

            <div class="collapse navbar-collapse" id="barra-navegacao">
                <ul class="nav navbar-nav navbar-right">
                    <li> <a href="index.php" id="inicio"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Início</a> </li>
                    <li> <a href="biografia.php" id="biografia"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>Biografia</a> </li>
                    <li> <a href="curiosidades.php" id="curiosidades"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Curiosidades</a> </li>
                    <li> <a href="obras-pensamentos.php" id="teorias"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>Obras e pensamentos</a> </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">