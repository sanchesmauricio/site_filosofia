<?php include "head.php"; ?>
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="page-header">
           <h2>Você está na página de curiosidades sobre Aristóteles</h2>
         </div>
            <br>
           <h4>Acompanhe abaixo algumas curiosidades <span class="glyphicon glyphicon-hand-down" aria-hidden="true"></span> </h4>
        
           <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title "><em>Seres vivos?</em></h3>
            </div>
            <div class="panel-body bg-info">
              Sim, Aristóteles tem a ver com os seres vivos! Até porque ele foi o <strong>pioneiro</strong> a classificá-los.
            </div>
          </div>
           


          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title "><em>Foi aluno de quem?</em></h3>
            </div>
            <div class="panel-body bg-info">
              Aristóteles além de ser um grande filósofo, ainda foi aluno de <strong>Alexandre o Grande</strong>.
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title "><em>O que ele achava das mulheres?</em></h3>
            </div>

            <div class="panel-body bg-info">
              Se você acha que vai ser algo animador, acabou de se enganar! A visão que Aristóteles tinha das mulheres
              era como <strong>"um homem incompleto"</strong>. Pelo fato de na reprodução a mulher ser passiva, e o homem ativo e produtivo. Ele pensava na mulher como o <strong>"campo"</strong>, e o homem como o <strong>"semeador".</strong>
            </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title "><em>Incluve insetos?</em></h3>
            </div>
            <div class="panel-body bg-info">
                Nosso grande filósofo <strong>estudou até os insetos</strong>, e descobriu que o corpo é constituído por três partes.
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title "><em>E as enciclopédias?</em></h3>
            </div>
            <div class="panel-body bg-info">
               Foi o <strong>primeiro enciclopedista</strong> da história, com conhecimentos abrangendo várias áreas. 
            </div>
            
<?php include "footer.php"; ?>