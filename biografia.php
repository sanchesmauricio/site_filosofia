<?php include "head.php"; ?>
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="page-header">
           <h2>Você está na página da Biografia.</h2>
         </div>
            <p class="inicio">
                <br>
        
           <h2>Vida de Aristóteles.</h2>
      
     
		
            <p class="inicio">Aristóteles nasceu em Estagira, na Macedônia, em 384 a.C. Com 17 anos, partiu para Atenas e começou a frequentar a Academia de Platão.
            De origem aristocrática, causou admiração pelo seu comportamento requintado e pela sua inteligência. Logo se tornou o discípulo predileto do mestre.</p>


            <p class="inicio">
            Com a morte de Platão, em 347 a.C., o brilhante e famoso aluno se considerava o substituto natural do mestre na direção da Academia. Porém, foi rejeitado e substituído por um ateniense nato.
            Decepcionado, deixou Atenas e partiu para Atarneus, na Ásia Menor – então grega. Foi conselheiro de estado de um antigo colega, o filósofo político Hermias.
            Casou com Pítria, filha adotiva de Hermias, mas quando os persas invadiram o país e mataram seu governante, ficou novamente sem pátria.</p>
            
            <p class="inicio">
            Em 343 a.C., foi convidado por Filipe II da Macedônia para preceptor de seu filho Alexandre. O rei queria que seu sucessor fosse um requintado filósofo. Assim, como preceptor na corte da Macedônia durante quatro anos, teve a oportunidade de prosseguir suas pesquisas e desenvolver muitas das suas teorias.
            Quando retornou a Atenas, em 335 a.C., Aristóteles decidiu fundar sua própria escola chamada Liceu por estar situada no edifício dedicado ao deus Apolo Lício.</p>
         
            <p class="inicio">
            Além dos cursos técnicos para os discípulos, dava aula ao povo em geral. No Liceu, estudava-se geometria, física, química, botânica, Astronomia, Matemática, etc.
            Em 323 a.C., com a morte de Alexandre Magno, rei da Macedônia, que então dominava a Grécia, Aristóteles foi acusado de ter apoiado o governo déspota e resolveu abandonar novamente Atenas.</p>
            
            <p class="inicio">
            Um ano depois, em 322 a.C., Aristóteles morreu em Cálcis, na Eubeia. Em seu testamento determinou a libertação dos seus escravos.
            A influência de Aristóteles sobre o desenvolvimento da Filosofia no mundo ocidental foi enorme, notadamente na Filosofia Cristã de São Tomás de Aquino durante a Idade Média. Sua influência se faz sentir até os nossos dias.</p>
            
          
        </div>

      <div class="panel-footer">Publicação feita no dia 11/05/2018 <p>Fonte: <a href="https://www.todamateria.com.br/aristoteles/" target="_blank">www.todamateria.com.br</a>
     </div> 

<?php include "footer.php"; ?>