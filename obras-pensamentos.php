<?php include "head.php"; ?>
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="page-header">
           <h2>Você está na página da Obras e pensamentos</h2>
       </div>
            <p class="inicio">
            
            <h2>Principais obras</h2>

            <ul id="ul"> <li>Ética a Nicômano (compilação de aulas de Aristóteles);</li>

            <li>Política;</li>

            <li>Organon;</li>

            <li>Retórica das Paixões;</li>

            <li>A poética clássica;</li>

            <li>Metafísica;</li>

            <li>De anima (Da alma);</li>

            <li>O homem de gênio e a melancolia;</li>

            <li>Magna Moralia (Grande Moral);</li>

            <li> Ética a Eudemo;</li>

            <li>Física;</li>

            <li>Sobre o Céu;</li>

            </ul>
           
        </div>
    </div>
</div>

        <div class="container">
         <div class="panel panel-default">
          <div class="panel-body">
        
            <h2>Frases</h2>

            <img class="irc_mi" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Aristotle_Altemps_Detail.jpg/200px-Aristotle_Altemps_Detail.jpg" alt="Resultado de imagem para aristoteles" onload="typeof google==='object'&amp;&amp;google.aft&amp;&amp;google.aft(this)" width="30" height="30" style="margin-right: 10px; float: left;">

            <p class="inicio"><b>“O segredo do sucesso é saber algo que ninguém mais sabe.”</b> - Aristóteles</p>

             <img class="irc_mi" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Aristotle_Altemps_Detail.jpg/200px-Aristotle_Altemps_Detail.jpg" alt="Resultado de imagem para aristoteles" onload="typeof google==='object'&amp;&amp;google.aft&amp;&amp;google.aft(this)" width="30" height="30" style="margin-right: 10px; float: left;">

             <p class="inicio"><b>“O ignorante afirma, o sábio duvida, o sensato reflete.” </b> - Aristóteles</p>
             <hr id="hr1">

               <img  class="irc_mi" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Aristotle_Altemps_Detail.jpg/200px-Aristotle_Altemps_Detail.jpg" alt="Resultado de imagem para aristoteles" onload="typeof google==='object'&amp;&amp;google.aft&amp;&amp;google.aft(this)" width="30" height="30" style="margin-right: 10px; float: left;">

              <p class="inicio"><b>“Sem amigos ninguém escolheria viver, mesmo que tivesse todos os outros bens”</b> - Aristóteles</p>

               <img  class="irc_mi" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Aristotle_Altemps_Detail.jpg/200px-Aristotle_Altemps_Detail.jpg" alt="Resultado de imagem para aristoteles" onload="typeof google==='object'&amp;&amp;google.aft&amp;&amp;google.aft(this)" width="30" height="30" style="margin-right: 10px; float: left;">
                <p class="inicio"><b>“O homem livre é senhor da sua vontade e escravo somente da sua consciência.” </b> - Aristóteles</p>

                <hr>

                <img  class="irc_mi" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Aristotle_Altemps_Detail.jpg/200px-Aristotle_Altemps_Detail.jpg" alt="Resultado de imagem para aristoteles" onload="typeof google==='object'&amp;&amp;google.aft&amp;&amp;google.aft(this)" width="30" height="30" style="margin-right: 10px; float: left;">

                <p class="inicio"><b>“O sábio nunca diz tudo o que pensa, mas pensa em tudo o que diz.”</b> - Aristóteles
                </p> 

                <img  class="irc_mi" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Aristotle_Altemps_Detail.jpg/200px-Aristotle_Altemps_Detail.jpg" alt="Resultado de imagem para aristoteles" onload="typeof google==='object'&amp;&amp;google.aft&amp;&amp;google.aft(this)" width="30" height="30" style="margin-right: 10px; float: left;">

                <p class="inicio"><b>“A dúvida é o principio da sabedoria.” </b> - Aristóteles</p>

        </div>
     </div>
     </div> 

      <div class="container">
         <div class="panel panel-default">
          <div class="panel-body">
        
            <h2>Pensamento</h2>

            <p class="inicio"> Segundo Aristóteles, a filosofia é <b>essencialmente teorética</b>: deve decifrar o enigma do universo, em face do qual a atitude inicial do espírito é o <b>assombro do mistério</b>. O seu problema fundamental é o problema do ser, não o problema da vida. O objeto próprio da filosofia, em que está a solução do seu problema, são as <b>essências imutáveis e a razão última das coisas</b>, isto é, o universal e o necessário, as formas e suas relações.

            <p class="inicio"> Entretanto, as formas são imanentes na experiência, nos indivíduos, de que constituem a essência. A filosofia aristotélica é, portanto, <b>conceptual</b> como a de Platão mas parte da experiência; é dedutiva, mas o ponto de partida da dedução é tirado - mediante o <b>intelecto da experiência</b>.</p>

            <p class="inicio"> A filosofia, pois, segundo Aristóteles, dividir-se-ia em <b>teorética, prática e poética</b>, abrangendo, destarte, todo o saber humano, racional. A teorética, por sua vez, divide-se em física, matemática e filosofia primeira (metafísica e teologia); a filosofia prática divide-se em ética e política; a poética em estética e técnica. </p>

            <p class="inicio"> Aristóteles é o <b>criador da lógica</b>, como ciência especial, sobre a base socrático-platônica; é denominada por ele analítica e representa a metodologia científica. Trata Aristóteles os problemas lógicos e gnosiológicos no conjunto daqueles escritos que tomaram mais tarde o nome de Órganon. Limitar-nos-emos mais especialmente aos problemas gerais da lógica de Aristóteles, porque aí está a sua gnosiologia.</p>

             <p class="inicio"> Foi dito que, em geral, a ciência, a filosofia - conforme Aristóteles, bem como segundo Platão - tem como objeto o <b>universal e o necessário</b>; pois não pode haver ciência em torno do individual e do contingente, conhecidos sensivelmente. Sob o ponto de vista metafísico, o objeto da ciência aristotélica é a <b>forma</b>, como idéia era o objeto da ciência platônica.</p> 
            </div>

      <div class="panel-footer">Publicação feita no dia 11/05/2018 <p>Fonte: <a href="http://www.pucsp.br/pos/cesima/schenberg/alunos/paulosergio/filosofia.html" target="_blank">www.pucsp.br</a>
     </div> 
     </div>

<?php include "footer.php"; ?>